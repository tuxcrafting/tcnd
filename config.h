// Regular font.
#define REGULAR_FONT \
	"-misc-fixed-medium-r-normal-*-15-140-75-75-c-90-iso10646-1"

// Bold font.
#define BOLD_FONT \
	"-misc-fixed-bold-r-normal-*-15-140-75-75-c-90-iso10646-1"

// Inside padding of a notification.
#define PADDING 5

// Border width.
#define BORDER_WIDTH 2

// Border color.
#define BORDER_COLOR "black"

// Anchoring of the window on the screen.
// This uses the virtual screen, so it may be out of bounds on heterogenous multihead setups.
// Can be ANCHOR_NW, ANCHOR_NE, ANCHOR_SW, ANCHOR_SE.
#define ANCHOR ANCHOR_NW

const char *BG_COLORS[3] = {
	// Background color for low urgency.
	"white",
	// Background color for medium urgency.
	"darkcyan",
	// Background color for high urgency.
	"darkred",
};

const char *FG_COLORS[3] = {
	// Foreground color for low urgency.
	"black",
	// Foreground color for medium urgency.
	"white",
	// Foreground color for high urgency.
	"white",
};

// A timeout value less than 0 means that the notification never expires.
const int TIMEOUTS[3] = {
	// Timeout for low urgency.
	6,
	// Timeout for medium urgency.
	10,
	// Timeout for high urgency.
	-1,
};
