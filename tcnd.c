#include <errno.h>
#include <libxnotif.h>
#include <poll.h>
#include <signal.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <X11/Xlib.h>

#define ANCHOR_NW 0
#define ANCHOR_NE 1
#define ANCHOR_SW 2
#define ANCHOR_SE 3

#include "config.h"

#if ANCHOR == ANCHOR_NW
#define ANCHOR_X(x, w) 0
#define ANCHOR_Y(y, h) 0
#elif ANCHOR == ANCHOR_NE
#define ANCHOR_X(x, w) w - x
#define ANCHOR_Y(y, h) 0
#elif ANCHOR == ANCHOR_SW
#define ANCHOR_X(x, w) 0
#define ANCHOR_Y(y, h) h - y
#elif ANCHOR == ANCHOR_SE
#define ANCHOR_X(x, w) w - x
#define ANCHOR_Y(y, h) h - y
#else
#error "Invalid anchor value"
#endif

static struct list_elm {
	struct xnotif_notification notification;
	int y1, y2;
	int ttl;
	char hold;
	struct list_elm *next;
} *notif_list;

// enqueue notification.
static void enqueue_notification(struct xnotif_notification *notification) {
	// find last.
	struct list_elm **elm = &notif_list;
	while (*elm != NULL) {
		elm = &(*elm)->next;
	}

	// create element.
	*elm = malloc(sizeof(struct list_elm));
	struct xnotif_notification *new_notif = &(*elm)->notification;
	(*elm)->next = NULL;

	// put notification.
	memcpy(new_notif, notification, sizeof(struct xnotif_notification));

	if (new_notif->title != NULL) {
		new_notif->title = strdup(new_notif->title);
	}
	new_notif->body = strdup(new_notif->body);

	(*elm)->ttl = TIMEOUTS[notification->urgency];
	(*elm)->hold = 0;
}

// unlink a notification.
static void unlink_notification(struct list_elm *prev, struct list_elm *elm) {
	// relink parent.
	if (prev == NULL) {
		notif_list = elm->next;
	} else {
		prev->next = elm->next;
	}

	// free element.
	if (elm->notification.title != NULL) { \
		free(elm->notification.title); \
	}
	free(elm->notification.body);
	free(elm);
}

// draw a string with a font.
static void draw_string(Display *display, Window window, int screen,
                        XFontStruct *font, char *string,
                        int x, int y,
                        int *width, int *height) {
	int len = strlen(string);
	int dir, ascent, descent;
	XCharStruct xch;
	XTextExtents(font, string, len, &dir, &ascent, &descent, &xch);
	*height = ascent + descent;
	*width = XTextWidth(font, string, len);
	XTextItem item = { 0 };
	item.chars = string;
	item.nchars = len;
	item.font = font->fid;
	XDrawText(display, window, DefaultGC(display, screen), x, y + ascent, &item, 1);
}

static char run = 1;
static char timer = 0;

static void int_handler(int sig) {
	if (sig == SIGALRM) {
		timer = 1;
	} else {
		run = 0;
	}
}

static unsigned long border_color;
static unsigned long bg_colors[3];
static unsigned long fg_colors[3];

int main(int argc, char *argv[]) {
	struct sigaction act = { 0 };
	act.sa_handler = int_handler;
	sigaction(SIGINT, &act, NULL);
	sigaction(SIGTERM, &act, NULL);
	sigaction(SIGALRM, &act, NULL);
	alarm(1);

	// get display.
	Display *display = XOpenDisplay(NULL);
	if (display == NULL) {
		fprintf(stderr, "Cannot open display\n");
		return 1;
	}
	int screen = DefaultScreen(display);

	// get colors.
#define GET_COLOR(src, dst)	  \
	XLookupColor(display, DefaultColormap(display, screen), \
	             src, &exact_c, &screen_c); \
	XAllocColor(display, DefaultColormap(display, screen), &screen_c); \
	dst = screen_c.pixel

	XColor exact_c, screen_c;
	GET_COLOR(BORDER_COLOR, border_color);
	for (int i = 0; i < 3; i++) {
		GET_COLOR(BG_COLORS[i], bg_colors[i]);
		GET_COLOR(FG_COLORS[i], fg_colors[i]);
	}

#undef GET_COLOR

	// create window.
	XSetWindowAttributes attributes;
	attributes.background_pixel = WhitePixel(display, screen);
	attributes.border_pixel = border_color;
	attributes.override_redirect = True;
	Window window = XCreateWindow(
		display, RootWindow(display, screen),
		0, 0, 1, 1, BORDER_WIDTH,
		CopyFromParent, CopyFromParent, CopyFromParent,
		CWBackPixel | CWBorderPixel | CWOverrideRedirect,
		&attributes);
	XStoreName(display, window, "tcnd");
	XSelectInput(display, window,
	             ExposureMask | ButtonPressMask |
	             ButtonReleaseMask | PointerMotionMask |
	             LeaveWindowMask | SubstructureRedirectMask);

	// load fonts.
	XFontStruct *regular_font = XLoadQueryFont(display, REGULAR_FONT);
	XFontStruct *bold_font = XLoadQueryFont(display, BOLD_FONT);

	// open XNotif socket.
	struct xnotif_server xnotif_server;
	enum xnotif_error err;
	if ((err = xnotif_server_open(&xnotif_server, -1)) != XNOTIF_SUCCESS) {
		XFreeFont(display, regular_font);
		XFreeFont(display, bold_font);
		XDestroyWindow(display, window);
		XCloseDisplay(display);
		fprintf(stderr, "Cannot open XNotif server: %s\n", xnotif_strerr(err));
		return 1;
	}

	struct pollfd fds[2] = { 0 };

	// X socket.
	fds[0].fd = ConnectionNumber(display);
	fds[0].events = POLLIN;

	// XNotif socket.
	fds[1].fd = xnotif_server.fd;
	fds[1].events = POLLIN;

	while (run) {
		// poll, exit on error.
		int r = poll(fds, 2, -1);
		if (r < 0) {
			if (errno != EINTR) {
				break;
			}
		}

		if (timer) {
			alarm(1);
			timer = 0;

			// decrement the time to live of all notifications except the selected one.
			// remove those with a TTL of 0.
			struct list_elm *elm = notif_list;
			struct list_elm *prev = NULL;
			while (elm != NULL) {
				if (elm->ttl == 0 && !elm->hold) {
					struct list_elm *next = elm->next;
					unlink_notification(prev, elm);
					XClearArea(display, window, 0, 0, 1, 1, True);
					XFlush(display);
					elm = next;
				} else {
					if (elm->ttl > 0 && !elm->hold) {
						elm->ttl--;
					}
					prev = elm;
					elm = elm->next;
				}
			}
		}

		if (fds[0].revents != 0) {
			// get event.
			XEvent event;
			XNextEvent(display, &event);

			switch (event.type) {
			case Expose: {
				// get window geometry.
				unsigned int w_width, w_height;
				Window _r;
				int _x, _y;
				unsigned int _b, _d;
				XGetGeometry(display, window, &_r,
				             &_x, &_y, &w_width, &w_height,
				             &_b, &_d);

				int max_width = 0;
				int y = 0;

				// get screen geometry.
				Screen *screen_p = ScreenOfDisplay(display, screen);
				int s_width = WidthOfScreen(screen_p);
				int s_height = HeightOfScreen(screen_p);

				// iterate on notifications.
				struct list_elm *elm = notif_list;
				while (elm != NULL) {
					// draw border.
					if (elm != notif_list) {
						XSetForeground(display, DefaultGC(display, screen),
						               border_color);
						XFillRectangle(display, window, DefaultGC(display, screen),
						               0, y, w_width, BORDER_WIDTH);
						y += BORDER_WIDTH;
					}

					elm->y1 = y;

					// do color.
					XSetForeground(display, DefaultGC(display, screen),
					               bg_colors[elm->notification.urgency]);
					XFillRectangle(display, window, DefaultGC(display, screen),
					               0, y, w_width, w_height - y);
					XSetForeground(display, DefaultGC(display, screen),
					               fg_colors[elm->notification.urgency]);

					int width, height;
					y += PADDING;
					int old_y = y;

					// draw title.
					if (elm->notification.title != NULL) {
						draw_string(display, window, screen,
						            bold_font, elm->notification.title,
						            PADDING * 2, y,
						            &width, &height);
						y += height;
						if (width > max_width) {
							max_width = width;
						}
					}

					// draw body.
					draw_string(display, window, screen,
					            regular_font, elm->notification.body,
					            PADDING * 2, y,
					            &width, &height);
					y += height + PADDING;
					if (width > max_width) {
						max_width = width;
					}

					// dither left side if selected.
					if (elm->hold) {
						for (int d_x = PADDING / 2;
						     d_x < PADDING + PADDING / 2;
						     d_x++) {
							for (int d_y = old_y;
							     d_y < y - PADDING;
							     d_y++) {
								if (d_x % 2 == d_y % 2) {
									XDrawPoint(display, window,
									           DefaultGC(display, screen),
									           d_x, d_y);
								}
							}
						}
					}

					elm->y2 = y;
					elm = elm->next;
				}
				if (y == 0) {
					// hide if there's no notifications to display.
					XUnmapWindow(display, window);
				} else {
					max_width += PADDING * 3;
					// make compiler error go away.
					if (s_width == s_height) {}
					// move and resize the window.
					XMoveResizeWindow(display, window,
					                  ANCHOR_X(max_width, s_width),
					                  ANCHOR_Y(y, s_height),
					                  max_width, y);
				}
				XFlush(display);
				break;
			}
			case ButtonRelease: {
				int y = event.xbutton.y;
				struct list_elm *elm = notif_list;
				struct list_elm *prev = NULL;
				while (elm != NULL) {
					// delete if it's the clicked notification.
					if (y >= elm->y1 && y <= elm->y2) {
						// unlink.
						unlink_notification(prev, elm);

						// redraw.
						XClearArea(display, window, 0, 0, 1, 1, True);
						XFlush(display);

						break;
					}
					prev = elm;
					elm = elm->next;
				}
				break;
			}
			case MotionNotify:
			case LeaveNotify: {
				int y = event.type == MotionNotify ? event.xmotion.y : -1;
				char refresh = 0;
				struct list_elm *elm = notif_list;
				while (elm != NULL) {
					char hold = elm->hold;
					if (y >= elm->y1 && y <= elm->y2) {
						// set hold if it's the selected notification.
						elm->hold = 1;
					} else {
						// else reset it.
						elm->hold = 0;
					}
					if (elm->hold != hold) {
						refresh = 1;
					}
					elm = elm->next;
				}
				if (refresh) {
					XClearArea(display, window, 0, 0, 1, 1, True);
					XFlush(display);
				}
				break;
			}
			}
		}

		if (fds[1].revents != 0) {
			// get notification.
			struct xnotif_notification notification;
			if (xnotif_server_recv(&xnotif_server, &notification) == XNOTIF_SUCCESS) {
				// enqueue it.
				enqueue_notification(&notification);

				XMapRaised(display, window);

				// redraw.
				XClearArea(display, window, 0, 0, 1, 1, True);
				XFlush(display);
			}
		}
	}

	// cleanup.
	xnotif_server_close(&xnotif_server);
	XFreeFont(display, regular_font);
	XFreeFont(display, bold_font);
	XDestroyWindow(display, window);
	XCloseDisplay(display);
	return 0;
}
