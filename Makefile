.POSIX:

CC=cc
CFLAGS=-Wall -Werror -O2
LDFLAGS=-lX11 -lxnotif
PREFIX=/usr/local
DESTDIR=
INSTALLDIR=$(DESTDIR)$(PREFIX)

tcnd: tcnd.c config.h
	$(CC) $< -o $@ $(CFLAGS) $(LDFLAGS)

clean:
	rm -f tcnd

install: tcnd
	mkdir -p $(INSTALLDIR)/bin
	cp tcnd $(INSTALLDIR)/bin

uninstall:
	rm -f $(INSTALLDIR)/bin/tcnd
