# tcnd

tcnd (TuxCrafting's Notification Daemon) is a simple notification daemon for [XNotif](https://gitlab.com/tuxcrafting/xnotif).

## Configuration

Configuration is done by modifying the `config.h` file.

## Bugs/Limitations

* UTF-8 isn't supported.
